<?php

namespace Drupal\genesis_csv_importer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\genesis_csv_importer\service\CandidatesImporterService;
use Drupal\genesis_csv_importer\service\CsvWorker;

class CsvImporterForm extends ConfigFormBase
{
    /**
     * @return array
     */
    protected function getEditableConfigNames()
    {
        return [
            'genesis_csv_importer.settings',
        ];
    }

    /**
     * @return string
     */
    public function getFormId()
    {
        return 'genesis_csv_importer_config_form';
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     * @return array
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Import Candidates to EP'),
            '#button_type' => 'primary'
        ];

        $form['#theme'] = 'system_config_form';

        return $form;
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * @param array $form
     * @param FormStateInterface $form_state
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        parent::submitForm($form, $form_state);
        $config = $this->config('genesis_csv_importer.settings');
        $isImported = $config->get('imported');

        if (!$isImported) {
            $candidates = CsvWorker::getCandidatesFromCSV();
            $constituencyData = CsvWorker::getConstituencyDataFromCSV();

            $importer = new CandidatesImporterService($candidates, $constituencyData);
            $importer->prepareStorage();
            $status = $importer->import();

            if ($status) {
                $config->set('imported', 1);
                $config->save();
                \Drupal::messenger()->addMessage(t('Candidates have been imported'));
            }
        } else {
            \Drupal::messenger()->addMessage(t('Candidates already imported'), MessengerInterface::TYPE_WARNING);
        }


    }
}
