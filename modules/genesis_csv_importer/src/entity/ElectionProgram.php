<?php

namespace Drupal\genesis_csv_importer\entity;

class ElectionProgram extends Dictionary
{
    /**
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->title = $title;
    }
}