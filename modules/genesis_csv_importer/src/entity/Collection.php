<?php

namespace Drupal\genesis_csv_importer\entity;

use Traversable;

class Collection implements \IteratorAggregate, \ArrayAccess
{
    /** @var string */
    private $type;

    /** @var Dictionary[]  */
    private $collection = [];

    /**
     * @param $type
     */
    public function __construct(string $type)
    {
        $this->type = $type;
    }

    /**
     * @inheritdoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->collection);
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->collection);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->collection[$offset];
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($value, $offset = null)
    {
        if ($value instanceof $this->type && is_null($offset)) {
            $this->collection[] = $value;
        } else if ($value instanceof $this->type && !$this->offsetExists($offset)) {
            $this->collection[$offset] = $value;
        }
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        if(is_array($offset)) {
            for ($i = $offset[0];$i<$offset[1];$i++) {
                unset($this->collection[$i]);
            }
        } else {
            unset($this->collection[$offset]);
        }
    }

    /**
     * @return array
     */
    public function getTargetIds(): array
    {
        $targetIds = [];

        /** @var Dictionary $item */
        foreach ($this->collection as $item) {
            if(!is_null($item->getTargetId())){
                $targetIds[] = $item->getTargetId();
            } else {
                \Drupal::logger('csv_import')->alert($item->getTitle());
            }
        }

        return $targetIds;
    }

    public function natsort()
    {
        natsort($this->collection);
    }

    public function asort()
    {
        asort($this->collection);
    }
}
