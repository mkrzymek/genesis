<?php

namespace Drupal\genesis_csv_importer\entity;

class Constituency extends Dictionary
{
    /** @var string */
    private $constituencyShortName;

    /** @var Collection */
    private $committeeCollection;

    /** @var int */
    private $votersNumber, $candidatesNumber, $constituencyNumber;

    /** @var Tag */
    private $tag;

    /**
     * @param string $title
     * @param string $constituencyShortName
     * @param Tag $tag
     * @param int $constituencyNumber
     * @param int $votersNumber
     * @param int $candidatesNumber
     */
    public function __construct(string $title, string $constituencyShortName, Tag $tag, int $constituencyNumber, int $votersNumber, int $candidatesNumber)
    {
        $this->title = $title;
        $this->constituencyShortName = $constituencyShortName;
        $this->constituencyNumber = $constituencyNumber;
        $this->committeeCollection = new Collection(ElectionCommittee::class);
        $this->votersNumber = $votersNumber;
        $this->candidatesNumber = $candidatesNumber;
        $this->tag = $tag;
    }

    /**
     * @return string
     */
    public function getConstituencyShortName(): string
    {
        return $this->constituencyShortName;
    }

    /**
     * @return Collection
     */
    public function getCommitteeCollection(): Collection
    {
        return $this->committeeCollection;
    }

    /**
     * @return int
     */
    public function getVotersNumber(): int
    {
        return $this->votersNumber;
    }

    /**
     * @return int
     */
    public function getCandidatesNumber(): int
    {
        return $this->candidatesNumber;
    }

    /**
     * @return int
     */
    public function getConstituencyNumber(): int
    {
        return $this->constituencyNumber;
    }

    /**
     * @return Tag
     */
    public function getTag(): Tag
    {
        return $this->tag;
    }
}