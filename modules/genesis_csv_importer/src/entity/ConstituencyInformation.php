<?php

namespace Drupal\genesis_csv_importer\entity;

class ConstituencyInformation
{
    const ARRAY_KEYS = [
        'constituencySeat',
        'constituencyNumber',
        'listNumber',
        'candidatesNumber',
        'citizens',
        'voters',
        'borders',
    ];

    /** @var int */
    private $votersNumber, $candidatesNumber, $constituencyNumber;

    /**
     * @param array $csvData
     */
    public function __construct(array $csvData)
    {
        $csvData = json_decode(json_encode(array_combine(SELF::ARRAY_KEYS, $csvData)));

        $this->votersNumber = (int)$csvData->voters;
        $this->candidatesNumber = (int)$csvData->candidatesNumber;
        $this->constituencyNumber = (int)$csvData->constituencyNumber;
    }

    /**
     * @return int
     */
    public function getVotersNumber(): int
    {
        return $this->votersNumber;
    }

    /**
     * @return int
     */
    public function getCandidatesNumber(): int
    {
        return $this->candidatesNumber;
    }

    /**
     * @return int
     */
    public function getConstituencyNumber(): int
    {
        return $this->constituencyNumber;
    }

    /**
     * @param int $votersNumber
     */
    public function setVotersNumber(int $votersNumber): void
    {
        $this->votersNumber = $votersNumber;
    }

    /**
     * @param int $candidatesNumber
     */
    public function setCandidatesNumber(int $candidatesNumber): void
    {
        $this->candidatesNumber = $candidatesNumber;
    }

    /**
     * @param int $constituencyNumber
     */
    public function setConstituencyNumber(int $constituencyNumber): void
    {
        $this->constituencyNumber = $constituencyNumber;
    }
}
