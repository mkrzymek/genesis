<?php

namespace Drupal\genesis_csv_importer\entity;

use Drupal\genesis_csv_importer\helper\PolishCharactersReplacer;

abstract class Dictionary extends PolishCharactersReplacer
{
    /** @var string */
    protected $sortTitle, $title;

    /** @var int */
    private $targetId;

    /**
     * Dictionary constructor.
     * @param string $title
     */
    public function __construct(string $title)
    {
        $this->sortTitle = $this->replacePolishChars($title);
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return int
     */
    public function getTargetId(): int
    {
        return $this->targetId;
    }

    /**
     * @param int $targetId
     */
    public function setTargetId(int $targetId): void
    {
        $this->targetId = $targetId;
    }

    public function __toString()
    {
        return $this->sortTitle;
    }
}