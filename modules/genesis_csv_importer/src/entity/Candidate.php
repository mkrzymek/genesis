<?php

namespace Drupal\genesis_csv_importer\entity;

class Candidate
{
    const ARRAY_KEYS = [
        'constituencyNumber',
        'constituencySeat',
        'committeeNumber',
        'committeeName',
        'committeeOrdinalNumber',
        'type',
        'candidateNumber',
        'surname',
        'name',
        'gender',
        'occupation',
        'home',
        'terytCode',
        'community',
        'isPartyMember',
        'support',

    ];

    const CONSTITUENCY_SEAT_ALTER = [
        'Gdańsk' => 'Gdańsku',
        'Bydgoszcz' => 'Bydgoszczy',
        'Olsztyn' => 'Olsztynie',
        'Warszawa' => 'Warszawie',
        'Łódź' => 'Łódzi',
        'Poznań' => 'Poznaniu',
        'Lublin' => 'Lublinie',
        'Rzeszów' => 'Rzeszowie',
        'Kraków' => 'Krakowie',
        'Katowice' => 'Katowicach',
        'Wrocław' => 'Wrocławiu',
        'Gorzów Wielkopolski' => 'Gorzowie Wielkopolskim',
    ];

    /** @var int */
    private $constituencyNumber, $committeeNumber, $candidateNumber;

    /** @var string */
    private $constituencySeat, $committeeName, $name, $occupation;

    /**
     * @param array $csvData
     */
    public function __construct(array $csvData)
    {
        $csvData = json_decode(json_encode(array_combine(SELF::ARRAY_KEYS, $csvData)));

        $this->constituencyNumber = (int)$csvData->constituencyNumber;
        $this->committeeNumber = (int)$csvData->committeeNumber;
        $this->candidateNumber = (int)$csvData->candidateNumber;
        $this->constituencySeat = $csvData->constituencySeat;
        $this->committeeName = $csvData->committeeName;
        $this->name = $csvData->surname . " " . $csvData->name;
        $this->occupation = $csvData->occupation;
    }

    /**
     * @return int
     */
    public function getConstituencyNumber(): int
    {
        return $this->constituencyNumber;
    }

    /**
     * @return int
     */
    public function getCommitteeNumber(): int
    {
        return $this->committeeNumber;
    }

    /**
     * @return int
     */
    public function getCandidateNumber(): int
    {
        return $this->candidateNumber;
    }

    /**
     * @return string
     */
    public function getConstituencySeat(): string
    {
        return $this->constituencySeat;
    }

    /**
     * @return string
     */
    public function getCommitteeName(): string
    {
        return $this->committeeName;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getOccupation(): string
    {
        return $this->occupation;
    }
}
