<?php

namespace Drupal\genesis_csv_importer\entity;

class MEP extends Dictionary
{
    /** @var ElectionCommittee */
    private $electionCommittee;

    /** @var string */
    private $occupation;

    /** @var int */
    private $listNumber;

    /** @var Tag */
    private $tag;

    /**
     * @param string $title
     * @param string $occupation
     * @param int $listNumber
     * @param ElectionCommittee $electionCommittee
     * @param Tag $tag
     */
    public function __construct(string $title, string $occupation, int $listNumber, ElectionCommittee $electionCommittee, Tag $tag)
    {
        $this->title = $title;
        $this->sortTitle = $listNumber . $this->replacePolishChars($title);
        $this->occupation = ucfirst($occupation);
        $this->listNumber = $listNumber;
        $this->electionCommittee = $electionCommittee;
        $this->tag = $tag;
    }

    /**
     * @return ElectionCommittee
     */
    public function getElectionCommittee(): ElectionCommittee
    {
        return $this->electionCommittee;
    }

    /**
     * @return string
     */
    public function getOccupation(): string
    {
        return $this->occupation;
    }

    /**
     * @return int
     */
    public function getListNumber(): int
    {
        return $this->listNumber;
    }

    /**
     * @return Tag
     */
    public function getTag(): Tag
    {
        return $this->tag;
    }
}