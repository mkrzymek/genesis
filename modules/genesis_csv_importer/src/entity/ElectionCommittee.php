<?php

namespace Drupal\genesis_csv_importer\entity;

use Drupal\node\Entity\Node;

class ElectionCommittee extends Dictionary
{
    /** @var Constituency */
    private $constituency;

    /** @var ElectionProgram */
    private $electionProgram;

    /** @var CommitteeLogo */
    private $committeeLogo;

    /** @var Collection */
    private $MEPCollection;

    /** @var int */
    private $committeeNumber;

    /** @var Tag */
    private $tag;

    /** @var Node */
    private $node;


    /**
     * @param string $title
     * @param int $committeeNumber
     * @param CommitteeLogo $committeeLogo
     * @param ElectionProgram $electionProgram
     * @param Tag $tag
     */
    public function __construct(
        string $title,
        int $committeeNumber,
        CommitteeLogo $committeeLogo,
        ElectionProgram $electionProgram,
        Tag $tag
    )
    {
        $title = $committeeNumber . '. ' . $title;
        parent::__construct($title);
        $this->MEPCollection = new Collection(MEP::class);
        $this->committeeNumber = $committeeNumber;
        $this->committeeLogo = $committeeLogo;
        $this->electionProgram = $electionProgram;
        $this->tag = $tag;
    }

    /**
     * @return Node
     */
    public function getNode(): Node
    {
        return $this->node;
    }

    /**
     * @param Node $node
     */
    public function setNode($node): void
    {
        $this->node = $node;
    }

    /**
     * @return Constituency
     */
    public function getConstituency(): Constituency
    {
        return $this->constituency;
    }

    /**
     * @return ElectionProgram
     */
    public function getElectionProgram(): ElectionProgram
    {
        return $this->electionProgram;
    }

    /**
     * @return CommitteeLogo
     */
    public function getCommitteeLogo(): CommitteeLogo
    {
        return $this->committeeLogo;
    }

    /**
     * @return Collection
     */
    public function getMEPCollection(): Collection
    {
        return $this->MEPCollection;
    }

    /**
     * @return int
     */
    public function getCommitteeNumber(): int
    {
        return $this->committeeNumber;
    }

    /**
     * @param Constituency $constituency
     */
    public function setConstituency(Constituency $constituency): void
    {
        $this->constituency = $constituency;
    }

    /**
     * @return Tag
     */
    public function getTag(): Tag
    {
        return $this->tag;
    }
}
