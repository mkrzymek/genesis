<?php

namespace Drupal\genesis_csv_importer\service;

use Drupal\genesis_csv_importer\entity\Candidate;
use Drupal\genesis_csv_importer\entity\ConstituencyInformation;

abstract class CsvWorker
{
    const PATH_TO_CANDIDATES_FILE = 'modules/genesis_csv_importer/csv/kandydaci.csv';
    const PATH_TO_CONSTITUENCY_INFO_FILE = 'modules/genesis_csv_importer/csv/okregi.csv';

    static public function getCandidatesFromCSV()
    {
        if (($file = fopen(self::PATH_TO_CANDIDATES_FILE, "r")) !== FALSE) {
            while (($csvData = fgetcsv($file, 1000, ";")) !== FALSE) {
                $candidates[] = new Candidate($csvData);
            }

            fclose($file);
        }
        array_shift($candidates);

        return $candidates;
    }

    static public function getConstituencyDataFromCSV()
    {
        $constituencyInfo = [];
        if (($file = fopen(self::PATH_TO_CONSTITUENCY_INFO_FILE, "r")) !== FALSE) {
            $candidatesSum = 0;
            $votersSum = 0;

            while (($csvData = fgetcsv($file, 1000, ";")) !== FALSE) {
                $constituencyInfoObj = new ConstituencyInformation($csvData);
                $constituencyInfo[] = $constituencyInfoObj;
                $candidatesSum += $constituencyInfoObj->getCandidatesNumber();
                $votersSum += $constituencyInfoObj->getVotersNumber();
            }

            /** @var ConstituencyInformation $generalInPoland */
            $generalInPoland = $constituencyInfo[CandidatesImporterService::GENERAL_IN_POLAND];
            $generalInPoland->setCandidatesNumber($candidatesSum);
            $generalInPoland->setConstituencyNumber(CandidatesImporterService::GENERAL_IN_POLAND);
            $generalInPoland->setVotersNumber($votersSum);

            fclose($file);
        }

        return $constituencyInfo;
    }
}