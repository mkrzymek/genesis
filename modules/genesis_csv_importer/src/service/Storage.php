<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 2019-04-24
 * Time: 10:12
 */

namespace Drupal\genesis_csv_importer\service;

use Drupal\genesis_csv_importer\entity\Collection;
use Drupal\genesis_csv_importer\entity\CommitteeLogo;
use Drupal\genesis_csv_importer\entity\Constituency;
use Drupal\genesis_csv_importer\entity\ElectionCommittee;
use Drupal\genesis_csv_importer\entity\ElectionProgram;
use Drupal\genesis_csv_importer\entity\MEP;
use Drupal\genesis_csv_importer\entity\Tag;

use Drupal\genesis_csv_importer\service\CandidatesImporterService;

class Storage
{
    /** @var Collection */
    private $logoCollection, $programCollection, $committeeCollection, $constituencyCollection, $MEPCollection, $tagsCollection;

    public function __construct()
    {
        $this->programCollection = new Collection(ElectionProgram::class);
        $this->logoCollection = new Collection(CommitteeLogo::class);
        $this->committeeCollection = new Collection(ElectionCommittee::class);
        $this->constituencyCollection = new Collection(Constituency::class);
        $this->MEPCollection = new Collection(MEP::class);
        $this->tagsCollection = new Collection(Tag::class);
        $this->tagsCollection->offsetSet(new Tag(
            CandidatesImporterService::GENERAL_IN_POLAND_TITLE),
            CandidatesImporterService::GENERAL_IN_POLAND
        );
    }

    /**
     * @return Collection
     */
    public function getLogoCollection(): Collection
    {
        return $this->logoCollection;
    }

    /**
     * @return Collection
     */
    public function getProgramCollection(): Collection
    {
        return $this->programCollection;
    }

    /**
     * @return Collection
     */
    public function getCommitteeCollection(): Collection
    {
        return $this->committeeCollection;
    }

    /**
     * @return Collection
     */
    public function getConstituencyCollection(): Collection
    {
        return $this->constituencyCollection;
    }

    /**
     * @return Collection
     */
    public function getMEPCollection(): Collection
    {
        return $this->MEPCollection;
    }

    /**
     * @return Collection
     */
    public function getTagsCollection(): Collection
    {
        return $this->tagsCollection;
    }

}