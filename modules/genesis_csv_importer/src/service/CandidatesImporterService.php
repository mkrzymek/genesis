<?php

namespace Drupal\genesis_csv_importer\service;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\Entity\Node;
use Drupal\genesis_csv_importer\entity\Candidate;
use Drupal\genesis_csv_importer\entity\Collection;
use Drupal\genesis_csv_importer\entity\CommitteeLogo;
use Drupal\genesis_csv_importer\entity\Constituency;
use Drupal\genesis_csv_importer\entity\ConstituencyInformation;
use Drupal\genesis_csv_importer\entity\ElectionCommittee;
use Drupal\genesis_csv_importer\entity\ElectionProgram;
use Drupal\genesis_csv_importer\entity\MEP;
use Drupal\genesis_csv_importer\entity\Tag;
use Drupal\taxonomy\Entity\Term;

class CandidatesImporterService
{
    const GENERAL_IN_POLAND = 0;
    const GENERAL_IN_POLAND_TITLE = "Polska ogółem";

    /** @var Candidate[] */
    private $candidates;

    /** @var ConstituencyInformation[] */
    private $constituencyInfo;

    /** @var Collection[] */
    private $storage;

    /**
     * @param array $candidates
     * @param array $constituencyInfo
     */
    public function __construct(array $candidates, array $constituencyInfo)
    {
        $this->candidates = $candidates;
        $this->constituencyInfo = $constituencyInfo;
        $this->storage = new Storage();
    }

    public function prepareStorage(): void
    {
        foreach ($this->candidates as $candidate) {

            $electionProgram = $this->prepareElectionProgram($candidate);
            $committeeLogo = $this->prepareElectionLogo($candidate);

            $tag = $this->prepareTag($candidate);
            $tagGeneral = $this->storage->getTagsCollection()->offsetGet(self::GENERAL_IN_POLAND);

            $committeeKey = $candidate->getConstituencyNumber() . $candidate->getCommitteeNumber();
            $generalCommitteeKey = self::GENERAL_IN_POLAND . $candidate->getCommitteeNumber();

            $generalElectionCommittee = $this->prepareElectionCommittee($candidate, $committeeLogo, $electionProgram, $generalCommitteeKey, $tag, true);
            $electionCommittee = $this->prepareElectionCommittee($candidate, $committeeLogo, $electionProgram, $committeeKey, $tag);

            $this->prepareConstituency($candidate, $generalElectionCommittee, $generalCommitteeKey, $tagGeneral);
            $this->prepareConstituency($candidate, $electionCommittee, $committeeKey, $tag, $candidate->getConstituencyNumber());

            $MEP = $this->prepareMEP($candidate, $electionCommittee, $tag);

            $this->storage->getCommitteeCollection()->offsetGet($committeeKey)->getMEPCollection()->offsetSet($MEP);
            $this->storage->getCommitteeCollection()->offsetGet($generalCommitteeKey)->getMEPCollection()->offsetSet($MEP);
        }
    }

    /**
     * @return bool
     */
    public function import(): bool
    {
        $transaction = \Drupal::database()->startTransaction();

        try {
            /** @var CommitteeLogo $logo */
            foreach ($this->storage->getLogoCollection() as $logo) {
                $term = Term::create(['vid' => 'logo_komitetow', 'name' => $logo->getTitle()]);
                $term->save();
                $logo->setTargetId($term->id());
            }

            /** @var ElectionProgram $program */
            foreach ($this->storage->getProgramCollection() as $program) {
                $term = Term::create(['vid' => 'program_komitetu', 'name' => $program->getTitle()]);
                $term->save();
                $program->setTargetId($term->id());
            }

            /** @var Tag $tag */
            foreach ($this->storage->getTagsCollection() as $tag) {
                $term = Term::create(['vid' => 'tags', 'name' => $tag->getTitle()]);
                $term->save();
                $tag->setTargetId($term->id());
            }

            /** @var ElectionCommittee $committee */
            foreach ($this->storage->getCommitteeCollection() as $committee) {
                $node = Node::create([
                    'type' => 'komitet',
                    'title' => $committee->getTitle(),
                    'field_numer_komitetu' => $committee->getCommitteeNumber(),
                    'field_logo_komitetu' => [$committee->getCommitteeLogo()->getTargetId()],
                    'field_program_komitetu' => [$committee->getElectionProgram()->getTargetId()],
                    'field_tags' => [$committee->getTag()->getTargetId()],
                ]);
                $node->save();
                $committee->setTargetId($node->id());
                $committee->setNode($node);
            }

            /** @var Constituency $constituency */
            foreach ($this->storage->getConstituencyCollection() as $constituency) {
                $constituency->getCommitteeCollection()->natsort();
                $node = Node::create([
                    'type' => 'okreg_wyboraczy',
                    'title' => $constituency->getTitle(),
                    'field_liczba_wyborcow' => $constituency->getVotersNumber(),
                    'field_liczba_kandydatow' => $constituency->getCandidatesNumber(),
                    'field_komitet_wyborczy_wiele' => $constituency->getCommitteeCollection()->getTargetIds(),
                    'field_numer_okregu' => $constituency->getConstituencyShortName(),
                    'field_numer' => $constituency->getConstituencyNumber(),
                    'field_tags' => [$constituency->getTag()->getTargetId()]
                ]);
                $node->save();
                $constituency->setTargetId($node->id());
            }

            /** @var MEP $MEP */
            foreach ($this->storage->getMEPCollection() as $MEP) {
                $node = Node::create([
                    'type' => 'europosel',
                    'title' => $MEP->getTitle(),
                    'field_komitet_wyborczy' => [$MEP->getElectionCommittee()->getTargetId()],
                    'field_pozycja_na_liscie' => $MEP->getListNumber(),
                    'field_tags' => [
                        $MEP->getTag()->getTargetId(),
                        $this->storage->getTagsCollection()->offsetGet(self::GENERAL_IN_POLAND)->getTargetId()
                    ],
                    'field_zawod' => $MEP->getOccupation(),
                ]);

                $node->save();
                $MEP->setTargetId($node->id());
            }

            /** @var ElectionCommittee $committee */
            foreach ($this->storage->getCommitteeCollection() as $committee) {
                $committee->getMEPCollection()->natsort();
                /** @var Node $node */
                $node = $committee->getNode();
                $node->set('field_europosel', $committee->getMEPCollection()->getTargetIds());
                $node->set('field_okreg_wyborczy', $committee->getConstituency()->getTargetId());

                $node->save();
            }
        } catch (\Exception $e) {
            $transaction->rollBack();

            \Drupal::logger('csv_importer')->alert('rollback!');
            \Drupal::logger('csv_importer')->alert($e->getMessage());
            \Drupal::messenger()->addMessage($e->getMessage(), MessengerInterface::TYPE_ERROR);

            return false;
        }

        return true;
    }

    /**
     * @param Candidate $candidate
     * @return ElectionProgram
     */
    private function prepareElectionProgram(Candidate $candidate): ElectionProgram
    {
        if ($this->storage->getProgramCollection()->offsetExists($candidate->getCommitteeNumber())) {
            $electionProgram = $this->storage->getProgramCollection()->offsetGet($candidate->getCommitteeNumber());
        } else {
            $electionProgram = new ElectionProgram($candidate->getCommitteeName());
        }
        $this->storage->getProgramCollection()->offsetSet($electionProgram, $candidate->getCommitteeNumber());

        return $electionProgram;
    }

    /**
     * @param Candidate $candidate
     * @return CommitteeLogo
     */
    private function prepareElectionLogo(Candidate $candidate): CommitteeLogo
    {
        if ($this->storage->getLogoCollection()->offsetExists($candidate->getCommitteeNumber())) {
            $committeeLogo = $this->storage->getLogoCollection()->offsetGet($candidate->getCommitteeNumber());
        } else {
            $committeeLogo = new CommitteeLogo($candidate->getCommitteeName());
        }
        $this->storage->getLogoCollection()->offsetSet($committeeLogo, $candidate->getCommitteeNumber());

        return $committeeLogo;
    }

    /**
     * @param Candidate $candidate
     * @return Tag
     */
    private function prepareTag(Candidate $candidate): Tag
    {
        if ($this->storage->getTagsCollection()->offsetExists($candidate->getConstituencyNumber())) {
            $tag = $this->storage->getTagsCollection()->offsetGet($candidate->getConstituencyNumber());
        } else {
            $tag = new Tag(sprintf("Okręg nr %s", $candidate->getConstituencyNumber()));
        }
        $this->storage->getTagsCollection()->offsetSet($tag, $candidate->getConstituencyNumber());

        return $tag;
    }

    /**
     * @param Candidate $candidate
     * @param CommitteeLogo $committeeLogo
     * @param ElectionProgram $electionProgram
     * @param string $committeeKey
     * @param Tag $tag
     * @param bool $general
     * @return ElectionCommittee|mixed
     */
    private function prepareElectionCommittee(
        Candidate $candidate,
        CommitteeLogo $committeeLogo,
        ElectionProgram $electionProgram,
        string $committeeKey,
        Tag $tag,
        bool $general = false
    )
    {
        if ($this->storage->getCommitteeCollection()->offsetExists($committeeKey)) {
            $electionCommittee = $this->storage->getCommitteeCollection()->offsetGet($committeeKey);
        } else {
            $tag = $general ? $this->storage->getTagsCollection()->offsetGet(self::GENERAL_IN_POLAND): $tag;
            $electionCommittee = new ElectionCommittee(
                $candidate->getCommitteeName(),
                $candidate->getCommitteeNumber(),
                $committeeLogo,
                $electionProgram,
                $tag
            );
        }
        $this->storage->getCommitteeCollection()->offsetSet($electionCommittee, $committeeKey);

        return $electionCommittee;
    }

    /**
     * @param Candidate $candidate
     * @param ElectionCommittee $electionCommittee
     * @param string $committeeKey
     * @param Tag $tag
     * @param int $constituencyKey
     */
    private function prepareConstituency(
        Candidate $candidate,
        ElectionCommittee $electionCommittee,
        string $committeeKey,
        Tag $tag,
        int $constituencyKey = self::GENERAL_IN_POLAND
    ): void
    {
        if ($this->storage->getConstituencyCollection()->offsetExists($constituencyKey)) {
            $constituency = $this->storage->getConstituencyCollection()->offsetGet($constituencyKey);
        } else {
            $title = sprintf("Okręg wyborczy nr %s w %s",
                $candidate->getConstituencyNumber(),
                Candidate::CONSTITUENCY_SEAT_ALTER[$candidate->getConstituencySeat()]
            );

            $title = $constituencyKey == self::GENERAL_IN_POLAND ? self::GENERAL_IN_POLAND_TITLE : $title;
            $constituencyShortName = $constituencyKey == self::GENERAL_IN_POLAND ? $title : "Okręg nr {$candidate->getConstituencyNumber()}";
            $constituencyNumber = $constituencyKey == self::GENERAL_IN_POLAND ? self::GENERAL_IN_POLAND : $candidate->getConstituencyNumber();

            $constituency = new Constituency(
                $title,
                $constituencyShortName,
                $tag,
                $constituencyNumber,
                $this->constituencyInfo[$constituencyKey]->getVotersNumber(),
                $this->constituencyInfo[$constituencyKey]->getCandidatesNumber()
            );
        }
        $constituency->getCommitteeCollection()->offsetSet($electionCommittee, $committeeKey);
        $electionCommittee->setConstituency($constituency);
        $this->storage->getConstituencyCollection()->offsetSet($constituency, $constituencyKey);
    }

    /**
     * @param Candidate $candidate
     * @param ElectionCommittee $electionCommittee
     * @param Tag $tag
     * @return MEP
     */
    private function prepareMEP(Candidate $candidate, ElectionCommittee $electionCommittee, Tag $tag): MEP
    {
        $MEP = new MEP(
            $candidate->getName(),
            $candidate->getOccupation(),
            $candidate->getCandidateNumber(),
            $electionCommittee,
            $tag
        );
        $this->storage->getMEPCollection()->offsetSet($MEP);

        return $MEP;
    }
}
