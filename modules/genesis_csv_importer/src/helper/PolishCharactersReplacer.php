<?php

namespace Drupal\genesis_csv_importer\helper;

class PolishCharactersReplacer
{
    const BASIC = ['ę', 'ó', 'ą', 'ś', 'Ś', 'ł', 'Ł', 'ż', 'Ż', 'ź', 'Ź', 'ń', 'Ń'];
    const TRANSLATABLE = ['ezz', 'ozz', 'azz', 'szz', 'Szz', 'lzz', 'Lzz', 'zz_', 'Zz_', 'zzz', 'Zzz', 'nzz', 'Nzz'];

    /**
     * @param string $str
     * @return string
     */
    public function replacePolishChars(string $str): string
    {
        return str_replace(self::BASIC, self::TRANSLATABLE, $str);
    }
}