<?php

namespace Drupal\amrestologic\Controller;

use Drupal\amrestologic\Services\ZipGenerator;
use Drupal\Core\Controller\ControllerBase;
use Drupal\file\FileInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class DocumentController extends ControllerBase
{
  private const ZIP_NAME = 'documents';

  /** @var ZipGenerator */
  private $zipGenerator;

  /**
   * DocumentController constructor.
   * @param ZipGenerator $zipGenerator
   */
  public function __construct(ZipGenerator $zipGenerator)
  {
    $this->zipGenerator = $zipGenerator;
  }

  /**
   * @param string $tidsCategory
   * @param int|null $tidBrand
   * @return Response
   */
  public function downloadFiles(string $tidsCategory, ?int $tidBrand = NULL): Response
  {

    $tids = explode('+', $tidsCategory);

    if ($files = $this->getFilesToZip($tids, $tidBrand)) {
      $zipUri = $this->zipGenerator->createZip(self::ZIP_NAME, ...$files);

      return $this->createResponse($zipUri);
    }

    throw new NotFoundHttpException();
  }

  /**
   * @param NodeInterface $node
   * @return Response
   */
  public function downloadFile(NodeInterface $node): Response
  {
    if ($file = $this->getFileToZip($node)) {
      $zipUri = $this->zipGenerator->createZip(self::ZIP_NAME, $file);

      return $this->createResponse($zipUri);
    }

    throw new NotFoundHttpException();
  }

  /**
   * @param array $tidsCategory
   * @param int|null $tidBrand
   * @return array
   */
  private function getFilesToZip(array $tidsCategory, ?int $tidBrand = NULL): array
  {
    $properties = [
      'type' => 'document',
      'field_category' => $tidsCategory,
    ];

    if ($tidBrand) {
      $properties['field_brand'] = $tidBrand;
    }

    $nodes = $this->entityTypeManager()->getStorage('node')->loadByProperties($properties);

    foreach ($nodes as $node) {
      if (!$node->access('view')) {
        throw new AccessDeniedHttpException();
      }
      $files[] = $this->getFileToZip($node);
    }

    return $files ?? [];
  }

  /**
   * @param NodeInterface $node
   * @return FileInterface
   */
  private function getFileToZip(NodeInterface $node): FileInterface
  {
    if (!$node->access('view')) {
      throw new AccessDeniedHttpException();
    }

    $mediaStorage = $this->entityTypeManager()->getStorage('media');
    $media = $mediaStorage->load($node->get('field_document')->getString());
    $fileStorage = $this->entityTypeManager()->getStorage('file');

    return $fileStorage->load($media->get('field_media_file')->getString());
  }

  /**
   * @param string $zipUri
   * @return Response
   */
  private function createResponse(string $zipUri): Response
  {
    $response = new Response();
    $response->headers->set('Cache-Control', 'private');
    $response->headers->set('Content-type', mime_content_type($zipUri));
    $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($zipUri) . '";');
    $response->headers->set('Content-length', filesize($zipUri));
    $response->setContent(file_get_contents($zipUri));

    return $response;
  }

  /**
   * @inheritDoc
   */
  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('amrestologic.zip_generator')
    );
  }
}
