<?php

namespace Drupal\genesis\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;

class ZipGenerator
{
  private const ZIP_DIR = 'zip/';

  /** @var \ZipArchive  */
  private $zipGenerator;

  /** @var FileSystemInterface  */
  private $fileSystem;

  /**
   * ZipGenerator constructor.
   * @param FileSystemInterface $fileSystem
   * @param \ZipArchive $zipArchive
   */
  public function __construct(FileSystemInterface $fileSystem, \ZipArchive $zipArchive)
  {
    $this->zipGenerator = $zipArchive;
    $this->fileSystem = $fileSystem;
  }

  /**
   * @param string $zipName
   * @param File ...$files
   * @return string
   */
  public function createZip(string $zipName, File ...$files): string
  {
    $zipDirectoryUri = file_default_scheme() . "://" . self::ZIP_DIR . $zipName;
    $zipAbsolutePath = $this->fileSystem->realpath($zipDirectoryUri);
    $this->fileSystem->prepareDirectory($zipDirectoryUri, $this->fileSystem::CREATE_DIRECTORY);
    $this->zipGenerator->open($zipAbsolutePath . "/$zipName.zip", \ZipArchive::OVERWRITE | \ZipArchive::CREATE);

    foreach ($files as $file) {
      $this->zipGenerator->addFile($this->fileSystem->realpath($file->getFileUri()), $file->getFilename());
    }

    $this->zipGenerator->close();

    return $zipDirectoryUri . "/$zipName.zip";
  }
}
